package com.example.demo.controllers;

import com.example.demo.entities.Task;
import com.example.demo.exceptions.ApiRequestException;
import com.example.demo.services.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping(path = "api/tasks")
public class TaskController {

    private final TaskService taskService;

    @Autowired
    public TaskController(TaskService taskService) {
        this.taskService = taskService;
    }

    @GetMapping(path = "{taskId}")
    public ResponseEntity<Task> getTask(@PathVariable("taskId") long id) {
        return new ResponseEntity<>(taskService.getTask(id)
            .orElseThrow(() -> new ApiRequestException("Task with ID: "+id+"doesn't exists.")), HttpStatus.OK);
    }

    @GetMapping(path = "all")
    public ResponseEntity<List<Task>> getAllTasks() {
        return new ResponseEntity<>(taskService.getAllTasks(), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<String> addNewTask(@RequestBody Task task) {
        taskService.addTask(task);
        return new ResponseEntity<String>("Task added", HttpStatus.OK);
    }

    @PutMapping(path = "{taskId}")
    public ResponseEntity<?> updateTask(@PathVariable("taskId") int id) {
        return taskService.getTask(id).map(task -> {
            taskService.updateTask(task.getId());
            return ResponseEntity.ok().build();
        }).orElseThrow(() -> new ApiRequestException("Task with ID: " + id + " doesn't exist."));
    }

    @DeleteMapping("{taskId}")
    public ResponseEntity<?> deleteTask(@PathVariable("taskId") Long id) {
        return taskService.getTask(id).map(task -> {
            taskService.deleteTask(task.getId());
            return ResponseEntity.ok().build();
        }).orElseThrow(() -> new ApiRequestException("Task with ID: " + id + " doesn't exist."));
    }
}
