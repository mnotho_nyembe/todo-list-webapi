package com.example.demo.entities;

import javax.persistence.*;

@Entity
@Table
public class Task {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String task;
    private String description;
    private boolean done;

    public Task(Long id, String task, String description, boolean done) {
        this.id = null;
        this.task = task;
        this.description = description;
        this.done = done;
    }

    public Task(String task, String description) {
        this.task = task;
        this.description = description;
    }

    public Task() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTask() {
        return task;
    }

    public void setTask(String task) {
        this.task = task;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isDone() {
        return done;
    }

    public void setDone() {
        this.done = !done;
    }
}
