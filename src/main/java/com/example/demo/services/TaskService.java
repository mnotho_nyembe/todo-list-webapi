package com.example.demo.services;

import com.example.demo.entities.Task;
import org.springframework.http.ResponseEntity;
import java.util.List;
import java.util.Optional;

public interface TaskService {

    List<Task> getAllTasks();

    Optional<Task> getTask(long id);

    void addTask(Task task);

    void updateTask(long id);

    void deleteTask(long id);
}
